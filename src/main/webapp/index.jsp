<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page session="true"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/resources/img/gear.ico"/>">
  

    <title>XFX</title>


<!-- <link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/bootstrap.min.css"/>"> -->
<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/bootstrap.css"/>">
 	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/cover.css"/>" >


  </head>
  
  <body onload='document.loginForm.username.focus();'>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

     
		<c:if test="${not empty error}">
			<div class="error"><h4 style="color: #d9534f">${error}</h4></div>
		</c:if>
<form class="form-horizontal" role="form" action="<c:url value='/j_spring_security_check' />" method='POST' name='loginForm'>

    <div class="col-sm-10">
      <input type="email" class="form-control" id="inputEmail3"  placeholder="Email" style=" width: 300px" name='j_username'  required autofocus>
    </div>
  
    <div class="col-sm-10">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password" style=" width: 300px" name='j_password' required autofocus>

  </div>
<br>
  <div class="form-group">
    <div class="col-xs-6 col-sm-3">
<button type="submit" name="submit" class="btn btn-default" id="login" style="width: 150px ;margin-right: 70px">LOGIN</button><a href="signup"><button type="button" class="btn btn-danger" >SING UP</button></a>
    </div>
  </div>
</form>
          
</div>
      </div>

    </div>



    <script src="<c:url value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/docs.min.js"/>"></script>
  </body>
</html>
