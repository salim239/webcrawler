package com.xfx.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="formulairex")
public class Formulaire {
	
	@Id
	@Column(name = "ID")
	private String id;
	@Column(name = "CHAMP")
	private String champ;
	@Column(name = "TITRE")
	private String titre;
	@Column(name = "CSSSELECTOR")
	private String cssSelector;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCssSelector() {
		return cssSelector;
	}
	public void setCssSelector(String cssSelector) {
		this.cssSelector = cssSelector;
	}
	public String getChamp() {
		return champ;
	}
	public void setChamp(String champ) {
		this.champ = champ;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}	
	
	
	
}
	