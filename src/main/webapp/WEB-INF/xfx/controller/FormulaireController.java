package com.xfx.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import com.xfx.model.Formulaire;
import com.xfx.model.Item;
import com.xfx.service.FormulaireService;
import com.xfx.service.ItemsService;

@Controller
public class FormulaireController {
	@Autowired
	private FormulaireService formulaireService;
	@Autowired
	private ItemsService itemsService;

	@RequestMapping(value = "/items", method = RequestMethod.GET)
	public String getPersonList(ModelMap model) {
		model.addAttribute("items", itemsService.listItem());
		return "output";
	}

	@RequestMapping(value = "/items/save", method = RequestMethod.POST)
	public View createPerson(@ModelAttribute Formulaire formulaire,
			ModelMap model) {
		org.jsoup.nodes.Document doc;
		List<Item> listItems = itemsService.listItem();

		try {
			System.setProperty("http.proxyHost", "192.168.3.254");
			System.setProperty("http.proxyPort", "3128");
			Connection connection = Jsoup.connect(formulaire.getChamp());
			doc = connection.get();
			Elements elements = doc.select(formulaire.getCssSelector());
			for (Element e : elements) {
				boolean test = false;
				x:for (Item i : listItems) {
					if (i.getDescription() == e.getElementsByClass("mbm")
							.text().trim()) {
						test = true;
						itemsService.updateItem(i);
						break x;
					}
				}
				if (!test){
					Item item=new Item();
				item.setDescription(e.getElementsByClass("mbm").text().trim());
				item.setPrix(e.getElementsByClass("value").text().trim());
				item.setTitre(formulaire.getTitre());
				System.out.println(item.getPrix()+item.getDescription());
				itemsService.addItem(item);
				formulaireService.addFormulaire(formulaire);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new RedirectView("/css/items");
	}

	@RequestMapping(value = "/items/delete", method = RequestMethod.GET)
	public View deletePerson(@ModelAttribute Item item, ModelMap model) {
		itemsService.removeItem(item);
		return new RedirectView("/css/items");
	}
	
	@RequestMapping(value = "/get_list", 
			method = RequestMethod.GET, 
			headers="Accept=*/*")
public @ResponseBody List<String> getList(@RequestParam("term") String query) {
		List<Formulaire> forms=formulaireService.listFormulaire();
		List<String> resultat=new ArrayList<String>();
		System.out.println(query);
		for(Formulaire e:forms) {
		if (e.getChamp().toLowerCase().startsWith(query.toLowerCase()))
			resultat.add(e.getChamp());}


return resultat;
}
	@RequestMapping(value = "/get_list_titre", 
			method = RequestMethod.GET, 
			headers="Accept=*/*")
public @ResponseBody List<String> getListTitre(@RequestParam("term") String query) {
		List<Formulaire> forms=formulaireService.listFormulaire();
		List<String> resultat=new ArrayList<String>();
		for(Formulaire e:forms) {
		if (e.getTitre().toLowerCase().startsWith(query.toLowerCase()))
			resultat.add(e.getTitre());}


return resultat;
}
	@RequestMapping(value = "/get_list_css", 
			method = RequestMethod.GET, 
			headers="Accept=*/*")
public @ResponseBody List<String> getListCssSelector(@RequestParam("term") String query) {
		List<Formulaire> forms=formulaireService.listFormulaire();
System.out.println(forms.size());
		unique(forms);
		System.out.println(forms.size());
		List<String> resultat=new ArrayList<String>();
		System.out.println(query);
		for(Formulaire e:forms) {
		if (e.getCssSelector().toLowerCase().startsWith(query.toLowerCase()))
			resultat.add(e.getCssSelector());}


return resultat;
}

	private void unique(List<Formulaire> forms) {
		if (forms.size()>1){
			for (int i = 1; i < forms.size(); i++) {
				x:for (int j = i; j < forms.size(); j++) {
				if (forms.get(i-1).getCssSelector().equals(forms.get(j).getCssSelector())) forms.remove(j);
				}
			}
		}
		System.out.println(forms.size()+"-----------");
	}

	
}
