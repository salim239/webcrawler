package com.xfx.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Item {

private String id;
private String titre;
private String description;
private String prix;

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getPrix() {
	return prix;
}
public void setPrix(String prix) {
	this.prix = prix;
}
public String getTitre() {
	return titre;
}
public void setTitre(String titre) {
	this.titre = titre;
}


}
