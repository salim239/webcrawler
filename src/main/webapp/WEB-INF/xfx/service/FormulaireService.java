package com.xfx.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.xfx.model.Formulaire;

@Repository
public class FormulaireService {
	@Autowired
	private MongoTemplate mongoTemplate;
	public static final String COLLECTION_NAME = "formulaire1";

	public void addFormulaire(Formulaire formulaire){
		if (!mongoTemplate.collectionExists(Formulaire.class)){
			mongoTemplate.createCollection(Formulaire.class);
		}
		formulaire.setId(UUID.randomUUID().toString());
		mongoTemplate.insert(formulaire,COLLECTION_NAME);
	}
	
	public List<Formulaire> listFormulaire(){
		return mongoTemplate.findAll(Formulaire.class,COLLECTION_NAME);
	}
	
	public void removeFormulaire(Formulaire formulaire){
		mongoTemplate.remove(formulaire, COLLECTION_NAME);
	}
	public void updateFormulaire(Formulaire formulaire){
		mongoTemplate.insert(formulaire, COLLECTION_NAME);
	}
	
}
