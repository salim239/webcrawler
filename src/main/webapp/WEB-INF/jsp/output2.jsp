<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/resources/img/gear.ico"/>">

    <title>XFX</title>

  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/justified-nav.css"/>" >
	        <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet" type="text/css" media="screen">
	     
	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/DT_bootstrap.css"/>" >
	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.css"/>" >
	<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/bootstrap.min.css"/>">
	</head>
	<script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/js/bootstrap.js"/>" type="text/javascript"></script>
	
 	<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery.dataTables.js"/>"></script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/DT_bootstrap.js"/>"></script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery-1.11.1.js"/>"></script>

		

<body >
	   <div class="container">
	   
	   <ul class="nav navbar-nav navbar-right">	   
	<li class="dropdown" style="float: right;">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">${authName}<span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="<c:url value="j_spring_security_logout" />">LogOut</a></li>
	          </ul>
	        </li>
	        </ul>
	  <br/>
	<br/>
	
	      <div class="masthead"  style=" width : 950px;">
	       
	        <ul class="nav nav-justified">
	          <li ><a href="/css/items">Home</a></li>
	          <li><a href="search">EXTRACT1</a></li>
	          <li><a href="#">EXTRACT2</a></li>
	 
	        </ul>
	      </div>
	      </div>
  <div class="row-fluid">
        <div class="span12">
<h1 align="center">Resultat</h1>
	   <div class="container">
    <table cellpadding="0" cellspacing="0" border="1" class="table table-striped table-bordered" id="elem">
                        
	 <thead>
	<tr>
	<th>ID</th> <th>titre</th> <th>description</th> <th>Action</th> 
	</tr>
	</thead>
	 <tbody>
		<c:forEach var="item" items="${items}">
			<tr>
		<td>${item.id}</td><td>${item.titre}</td><td>${item.description}</td><td><div align="center"><input type="button" value="delete" class="btn btn-danger"  onclick="window.location='items/delete?id=${item.id}'"/></div></td>
			</tr>
		</c:forEach>
		 </tbody>
	</table>
<a href="itemsX">
<img src="<spring:url value="/resources/img/xml.png" htmlEscape="true" />" alt="XML" height="20" width="20"/>
</a>


<a href="itemsY">
<img src="<spring:url value="/resources/img/json-icon.png" htmlEscape="true" />" alt="XML" height="20" width="20"/>
</a>
<a href="downloadPDF">
<img src="<spring:url value="/resources/img/pdf.png" htmlEscape="true" />" alt="XML" height="20" width="20"/>
</a>
</div>

</div>
</div>


</body>
</html>