<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Singup</title>
        
        <!-- The stylesheet -->
        <link rel="stylesheet" href="<c:url value="/resources/css/styles.css"/>" />
        
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <div id="main">
        
        	<h1>Sign up</h1>
        	
        	<form class="" method="post" action="signup/createaccount">
        		
        		<div class="row email">
	    			<input type="text" id="email" name="email" placeholder="Email" />
        		</div>
        		
        		<div class="row pass">
        			<input type="password" id="password1" name="password" placeholder="Password" />
        		</div>
        		
        		<div class="row pass">
        			<input type="password" id="password2" name="password2" placeholder="Password (repeat)" disabled="true" />
        		</div>
        		
        		<!-- The rotating arrow -->
        		<div class="arrowCap"></div>
        		<div class="arrow"></div>
        		
        		<p class="meterText">Password Meter</p>
        		
        		<input type="submit" value="Register" />
        		
        	</form>
        </div>
        
      
        
        <!-- JavaScript includes - jQuery, the complexify plugin and our own script.js -->
		<script src="<c:url value="http://code.jquery.com/jquery-1.7.2.min.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery.complexify.js"/>"></script>
		<script src="<c:url value="/resources/js/script.js"/>"></script>
		     
    </body>
</html>