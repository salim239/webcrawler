<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/resources/img/gear.ico"/>">

    <title>XFX</title>


<!-- <link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/bootstrap.min.css"/>"> -->
<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/bootstrap.min.css"/>">
 	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/cover.css"/>" >

  </head>

  <body>
  
 <div class="bs-example" style="width: 500px;margin-left: 30%;margin-bottom: 10%" >
      <form data-toggle="validator" role="form" method="post" action="signup/createaccount">
    
<h1>Create an account</h1>
  
        <div class="form-group">
       
          <input type="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Bruh, that email address is invalid" name="email"  required>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
          <div class="form-inline row">
            <div class="form-group col-sm-6">
              <input type="password" data-toggle="validator" data-minlength="6" class="form-control" id="inputPassword" placeholder="Password" name="password"  required>
              <span class="help-block">Minimum of 6 characters</span>
            </div>
            <div class="form-group col-sm-6">
              <input type="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm" required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
        </div>
     
        <div class="form-group">
          <div class="checkbox">
            <label>
              <input type="checkbox" id="terms" data-error="checkbox" required>
             i agree
            </label>
            <div class="help-block with-errors"></div>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div> <!-- /example -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<c:url value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/docs.min.js"/>"></script>
     <script src="<c:url value="/resources/js/validator.min.js"/>"></script>
  </body>
</html>
