<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/resources/img/gear.ico"/>">

    <title>XFX</title>

  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/justified-nav.css"/>" >
	        <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet" type="text/css" media="screen">
	     
	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/DT_bootstrap.css"/>" >
	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.css"/>" >
	<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/bootstrap.min.css"/>">
	</head>
	<script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/js/bootstrap.js"/>" type="text/javascript"></script>
	
 	<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery.dataTables.js"/>"></script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/DT_bootstrap.js"/>"></script>
	<!--<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery-1.11.1.js"/>"></script>-->

		<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
 <script type="text/javascript">
	function split(val) {
	    return val.split(/,\s*/);
	}
	function extractLast(term) {
	    return split(term).pop();
	}
		
$(document).ready(function() {

	$( "#champ" ).autocomplete({
		source: '${pageContext. request. contextPath}/get_list'
	});
	$( "#titre" ).autocomplete({
		source: '${pageContext. request. contextPath}/get_list_titre'
	});
	$( "#cssSelector" ).autocomplete({
		source: '${pageContext. request. contextPath}/get_list_css'
	});
});
</script>
 
<body >
   <div class="container">
   
   <ul class="nav navbar-nav navbar-right">	   
<li class="dropdown" style="float: right;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">${authName} <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="j_spring_security_logout" />">LogOut</a></li>
          </ul>
        </li>
        </ul>
  <br/>
<br/>

      <div class="masthead" style=" width : 950px;">
       
        <ul class="nav nav-justified">
          <li class="active"><a href="items">Home</a></li>
          <li><a href="items/search">EXTRACT1</a></li>
          <li><a href="items/extract2">EXTRACT2</a></li>
 
        </ul>
      </div>
      </div>
  <div class="row-fluid">
        <div class="span12">

<div>
<br/>
<br/>
<form action="items/searchi" method="post">
 <div class="form-group has-success" align="center">
 
<div>  <input type="text" class="form-control" id="inputSuccess1" name="url"  style="width: 600;height: 30"></div>
  <button type="submit" class="btn btn-primary" style="width: 600">Search</button>
</div>
</form>
</div>

<br/>
<br/>
	   <div class="container">
    <table cellpadding="0" cellspacing="0" border="1" class="table table-striped table-bordered" id="example">
                        
	 <thead>
	<tr>
	<th>ID</th> <th>titre</th> <th>description</th> <th>Action</th> 
	</tr>
	</thead>
	 <tbody>
		<c:forEach var="item" items="${items}">
			<tr>
		<td>${item.id}</td><td>${item.titre}</td><td>${item.description}</td><td><div align="center"><input type="button" class="btn btn-danger" value="delete" onclick="window.location='items/delete?id=${item.id}'"/></div></td>
			</tr>
		</c:forEach>
		 </tbody>
	</table>
</div>

</div></div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>