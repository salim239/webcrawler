<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/resources/img/gear.ico"/>">

    <title>XFX</title>

  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/justified-nav.css"/>" >
	        <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet" type="text/css" media="screen">
	     
	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/DT_bootstrap.css"/>" >
	        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.css"/>" >
	<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/bootstrap.min.css"/>">
	</head>
	<script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/js/bootstrap.js"/>" type="text/javascript"></script>
	
 	<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery.dataTables.js"/>"></script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/DT_bootstrap.js"/>"></script>
	<!--<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery-1.11.1.js"/>"></script>-->

		<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
		<script type="text/javascript" charset="utf-8" language="javascript" src="<c:url value="/resources/js/bootstrap.file-input.js"/>"></script>
 <script type="text/javascript">
	function split(val) {
	    return val.split(/,\s*/);
	}
	function extractLast(term) {
	    return split(term).pop();
	}
		
$(document).ready(function() {

	$( "#champ" ).autocomplete({
		source: '${pageContext. request. contextPath}/get_list'
	});
	$( "#titre" ).autocomplete({
		source: '${pageContext. request. contextPath}/get_list_titre'
	});
	$( "#cssSelector" ).autocomplete({
		source: '${pageContext. request. contextPath}/get_list_css'
	});
});
</script>
 
<body >
<body>
  <div class="container">
   
   <ul class="nav navbar-nav navbar-right">	   
<li class="dropdown" style="float: right;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">${authName}<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<c:url value="/j_spring_security_logout" />">LogOut</a></li>
          </ul>
        </li>
        </ul>
  <br/>
<br/>

      <div class="masthead" style=" width : 950px;">
       
        <ul class="nav nav-justified">
          <li ><a href="/css/items">Home</a></li>
          <li><a href="search">EXTRACT1</a></li>
          <li class="active"><a href="#">EXTRACT2</a></li>
 
        </ul>
      </div>
      </div>

	   <div class="container">
	     <div class="row-fluid">
        <div class="span12">

<br/><br/>
	     <form method="POST" action="extract2/uploadFile" enctype="multipart/form-data">
	  <div style="position:relative;">
		<a class='btn btn-primary' href='javascript:;'>
			Choose File...
			<input type="file" name="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
		</a>
		&nbsp;
		<span class='label label-info' id="upload-file-info" ></span>
	</div>
	<br/>
        <input type="submit" value="Upload" class="btn btn-success" style=" width : 111px;"> 
    </form>
    
		</div></div>
		<br><br>
    <table cellpadding="0" cellspacing="0" border="1" class="table table-striped table-bordered" id="example">
                        
	 <thead>
	<tr>
	<th>ID</th> <th>titre</th> <th>description</th> 
	</tr>
	</thead>
	 <tbody>
		<c:forEach var="item" items="${items}">
			<tr>
		<td>${item.id}</td><td>${item.titre}</td><td>${item.description}</td>
			</tr>
		</c:forEach>
		 </tbody>
	</table>
</div>
</html>