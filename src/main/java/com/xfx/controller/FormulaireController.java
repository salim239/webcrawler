package com.xfx.controller;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.csvreader.CsvReader;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.xfx.model.Formulaire;
import com.xfx.model.Item;
import com.xfx.model.ItemsX;
import com.xfx.model.ItemsY;
import com.xfx.model.Role;
import com.xfx.model.UserAccount;
import com.xfx.service.FormulaireService;
import com.xfx.service.ItemsService;
import com.xfx.service.RoleService;
import com.xfx.service.UserAccountService;

@Controller
public class FormulaireController {
	@Autowired
	private FormulaireService formulaireService;
	@Autowired
	private ItemsService itemsService;

	@Autowired
	private RoleService roleService;
	@Autowired
	private UserAccountService userAccountService;

	private List<Item> ii;
	private List<Item> iisearsh = new ArrayList<Item>();
	private List<Item> iisearsh2 = new ArrayList<Item>();
	private List<Item> iisearsh3 = new ArrayList<Item>();

	@RequestMapping(value = "items/extract2", method = RequestMethod.GET)
	public String getExtract2(ModelMap model) {
		model.addAttribute("items", iisearsh2);
		model.addAttribute("items", iisearsh3);
		model.addAttribute("authName", SecurityContextHolder.getContext()
				.getAuthentication().getName());
		return "extract2";
	}

	@RequestMapping(value = "items/search", method = RequestMethod.GET)
	public String getPersonList(ModelMap model) {
		model.addAttribute("items", iisearsh);
		model.addAttribute("authName", SecurityContextHolder.getContext()
				.getAuthentication().getName());
		return "search";
	}

	@RequestMapping(value = "/items", method = RequestMethod.GET)
	public String getHome(ModelMap model) {
		model.addAttribute("items", itemsService.listItem());

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		model.addAttribute("authName", auth.getName());
		return "output";
	}

	@RequestMapping(value = "/items/searchi", method = RequestMethod.POST)
	public String search(@RequestParam("url") String url, ModelMap model) {
		ii = itemsService.getListByUrl(url);
		model.addAttribute("items", ii);
		model.addAttribute("auth", SecurityContextHolder.getContext()
				.getAuthentication().getName());
		return "output2";
	}

	@RequestMapping(value = "/items/save", method = RequestMethod.POST)
	public View createPerson(@ModelAttribute Formulaire formulaire,
			ModelMap model) {
		org.jsoup.nodes.Document doc;
		List<Item> listItems = itemsService.listItem();

		try {

			System.setProperty("http.proxyHost", "192.168.3.254");
			System.setProperty("http.proxyPort", "3128");

			Connection connection = Jsoup.connect(formulaire.getChamp());
			doc = connection.get();
			Elements elements = doc.select(formulaire.getCssSelector());
			Item item = new Item();
			item.setDescription(elements.text());
			item.setTitre(formulaire.getTitre());
			item.setUrl(formulaire.getChamp());
			itemsService.addItem(item);
			formulaireService.addFormulaire(formulaire);
			iisearsh.add(item);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new RedirectView("/css/items");
	}

	@RequestMapping(value = "/items/search/save", method = RequestMethod.POST)
	public View createPersoni(@ModelAttribute Formulaire formulaire,
			ModelMap model) {
		org.jsoup.nodes.Document doc;
		List<Item> listItems = itemsService.listItem();

		try {

			System.setProperty("http.proxyHost", "192.168.3.254");
			System.setProperty("http.proxyPort", "3128");

			Connection connection = Jsoup.connect(formulaire.getChamp());
			doc = connection.get();
			Elements elements = doc.select(formulaire.getCssSelector());
			Item item = new Item();
			item.setDescription(elements.text());
			item.setTitre(formulaire.getTitre());
			item.setUrl(formulaire.getChamp());
			itemsService.addItem(item);
			formulaireService.addFormulaire(formulaire);
			iisearsh.add(item);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new RedirectView("/css/items/search");
	}

	@RequestMapping(value = "/csv", method = RequestMethod.POST)
	public View saveCsv(@RequestParam("textarea1") String text, ModelMap model,
			HttpServletRequest request) {
		InputStream inputStream = null;
		BufferedReader entree = new BufferedReader(new StringReader(text));
		try {

			Part filePart = request.getPart("file");

			if (filePart != null) {
				// prints out some information for debugging
				System.out.println(filePart.getName());
				System.out.println(filePart.getSize());
				System.out.println(filePart.getContentType());

				// obtains input stream of the upload file
				inputStream = filePart.getInputStream();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {

			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}

		return new RedirectView("/css/output3");
	}

	@RequestMapping(value = "/items/delete", method = RequestMethod.GET)
	public View deletePerson(@ModelAttribute Item item, ModelMap model) {
		itemsService.removeItem(item);
		return new RedirectView("/css/items");
	}

	@RequestMapping(value = "get_list", method = RequestMethod.GET, headers = "Accept=*/*")
	public @ResponseBody
	List<String> getList(@RequestParam("term") String query) {
		List<Formulaire> forms = formulaireService.listFormulaire();
		List<String> resultat = new ArrayList<String>();
		for (Formulaire e : forms) {
			if (e.getChamp().toLowerCase().startsWith(query.toLowerCase()))
				resultat.add(e.getChamp());
		}

		return resultat;
	}

	@RequestMapping(value = "get_list_titre", method = RequestMethod.GET, headers = "Accept=*/*")
	public @ResponseBody
	List<String> getListTitre(@RequestParam("term") String query) {
		List<Formulaire> forms = formulaireService.listFormulaire();
		List<String> resultat = new ArrayList<String>();

		for (Formulaire e : forms) {
			if (e.getTitre().toLowerCase().startsWith(query.toLowerCase()))
				resultat.add(e.getTitre());
		}

		return resultat;
	}

	@RequestMapping(value = "get_list_css", method = RequestMethod.GET, headers = "Accept=*/*")
	public @ResponseBody
	List<String> getListCssSelector(@RequestParam("term") String query) {
		List<Formulaire> forms = formulaireService.listFormulaire();
		System.out.println(forms.size());
		unique(forms);
		List<String> resultat = new ArrayList<String>();
		System.out.println(query);
		for (Formulaire e : forms) {
			if (e.getCssSelector().toLowerCase()
					.startsWith(query.toLowerCase()))
				resultat.add(e.getCssSelector());
		}

		return resultat;
	}

	private void unique(List<Formulaire> forms) {
		if (forms.size() > 1) {
			for (int i = 1; i < forms.size(); i++) {
				x: for (int j = i; j < forms.size(); j++) {
					if (forms.get(i - 1).equals(forms.get(j)))
						forms.remove(j);
				}
			}
		}

	}

	@RequestMapping(value = "/items/itemsX", method = RequestMethod.GET)
	public @ResponseBody
	ItemsX getItemsInXML() {
		ItemsX i = new ItemsX();
		System.out.println(ii);
		i.setItems(ii);

		return i;

	}

	@RequestMapping(value = "/items/itemsY", method = RequestMethod.GET)
	public @ResponseBody
	ItemsY getItemIn() {
		ItemsY i = new ItemsY();
		i.setItems(ii);

		return i;

	}

	@RequestMapping(value = "/items/downloadPDF", method = RequestMethod.GET)
	public @ResponseBody
	void main(HttpServletResponse response) throws IOException,
			DocumentException {

		Document document = new Document();

		PdfWriter.getInstance(document, new FileOutputStream("Items.pdf"));

		document.open();

		document.add(createFirstTable());
		document.close();

		File file = new File("Items.pdf");

		ServletOutputStream stream = null;
		BufferedInputStream buf = null;
		try {
			stream = response.getOutputStream();
			// set response headers
			response.setContentType("application/pdf");
			response.setDateHeader("Expires", 0);
			response.addHeader("Content-Disposition",
					"inline; filename=dwg.pdf");
			response.setContentLength((int) file.length());
			buf = new BufferedInputStream(new FileInputStream(file));
			int readBytes = 0;
			while ((readBytes = buf.read()) != -1)
				stream.write(readBytes);
		} finally {
			if (stream != null)
				stream.flush();
			stream.close();
			if (buf != null)
				buf.close();
		}
	}

	public PdfPTable createFirstTable() {

		PdfPTable table = new PdfPTable(3);

		PdfPCell cell;

		cell = new PdfPCell(new Phrase("List of Items"));
		cell.setColspan(3);
		table.addCell(cell);

		table.addCell("ID");
		table.addCell("Titre");
		table.addCell("Description");

		for (int i = 0; i < ii.size(); i++) {

			table.addCell(ii.get(i).getId());
			table.addCell(ii.get(i).getTitre());
			table.addCell(ii.get(i).getDescription());

		}
		return table;

	}

	@RequestMapping(value = "/output3", method = RequestMethod.GET)
	public String output3() {

		return "output3";
	}

	@RequestMapping(value = "items/extract2/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	View uploadFileHandler(@RequestParam("file") MultipartFile file)
			throws IOException {
		String chaine = "";
		String name = "f";
		InputStream inputStream = file.getInputStream();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));

		String line;
		while ((line = bufferedReader.readLine()) != null) {
			System.out.println(line);
		}

		try {

			CsvReader products = new CsvReader(new InputStreamReader(
					file.getInputStream()));

			products.readHeaders();

			while (products.readRecord()) {
				int x=products.getColumnCount()/3;
				for (int i = 0; i < x; i++){
				String url = products.get(i*3+1);
				String titre = products.get(i*3+2);
				String cssSelector = products.get(i*3+3);

				org.jsoup.nodes.Document doc;
				List<Item> listItems = itemsService.listItem();

				try {
					System.setProperty("http.proxyHost", "192.168.3.254");
					System.setProperty("http.proxyPort", "3128");
					Connection connection = Jsoup.connect(url);
					doc = connection.get();
					Elements elements = doc.select(cssSelector);
					Item item = new Item();
					item.setDescription(elements.text());
					item.setTitre(titre);
					item.setUrl(url);
					itemsService.addItem(item);
					iisearsh3.add(item);
					Formulaire formulaire = new Formulaire();
					formulaire.setChamp(url);
					formulaire.setTitre(titre);
					formulaire.setCssSelector(cssSelector);
					formulaireService.addFormulaire(formulaire);

				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println(url + ":" + titre + ":" + cssSelector);
			}
		}
			products.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new RedirectView("/css/items/extract2");

	}

	@RequestMapping(value = "/op", method = RequestMethod.GET)
	public @ResponseBody
	void op() {
		Role role = new Role("ROLE_ADMIN");
		Role role1 = new Role("ROLE_USER");

		roleService.addRole(role);
		roleService.addRole(role1);
		System.out.println("done");

	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup() {
		return "singup";

	}

	@RequestMapping(value = "/signup/createaccount", method = RequestMethod.POST)
	public View createCompte(@ModelAttribute UserAccount userAccount,
			ModelMap model) {

		userAccount.getRoles().add(new Role("ROLE_USER"));
		userAccount.setEnabled(true);
		userAccountService.addUserAccount(userAccount);
		return new RedirectView("/css");
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid Authentication");
		}
		model.setViewName("login");

		return model;

	}

}
