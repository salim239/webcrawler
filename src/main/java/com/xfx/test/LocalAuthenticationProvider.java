package com.xfx.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.security.core.GrantedAuthority;
import com.xfx.model.UserAccount;
import com.xfx.service.UserAccountService;

@Component
public class LocalAuthenticationProvider extends
		AbstractUserDetailsAuthenticationProvider {
	@Autowired
	UserAccountService userAccountService;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
	}

	@Override
	protected UserDetails retrieveUser(String username,
			UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		String password = (String) authentication.getCredentials();
		List<GrantedAuthority> auths = AuthorityUtils.NO_AUTHORITIES;
		List<UserAccount> list = userAccountService.listUserAccount();
		boolean test = false;
		for (UserAccount e : list) {
			if (username.equals(e.getEmail())) {
				if (password.equals(e.getPassword())) {
					test = true;
					auths = AuthorityUtils
							.commaSeparatedStringToAuthorityList("ROLE_USER");
					break;
				}
			}
		}

		return new User(username, password, test, // enabled
				true, // account not expired
				true, // credentials not expired
				true, // account not locked
				auths);

	}

}
