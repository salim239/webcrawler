package com.xfx.model;

import java.util.ArrayList;
import java.util.List;

public class ItemsY {

	private List<Item> items;

	public List<Item> getItems() {
		if (items == null)
			return items = new ArrayList<Item>();
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

}
