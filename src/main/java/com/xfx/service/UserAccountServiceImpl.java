package com.xfx.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xfx.dao.UserAccountDao;
import com.xfx.model.UserAccount;

@Repository
public class UserAccountServiceImpl implements UserAccountService {
	@Autowired
	private UserAccountDao userAccountDao;

	public void addUserAccount(UserAccount userAccount) {
		userAccountDao.addUserAccount(userAccount);
	}

	public List<UserAccount> listUserAccount() {
		return userAccountDao.listUserAccount();
	}

	public void removeUserAccount(UserAccount userAccount) {
		userAccountDao.removeUserAccount(userAccount);
	}

	public void updateUserAccount(UserAccount userAccount) {
		userAccountDao.updateUserAccount(userAccount);
	}

}
