package com.xfx.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xfx.dao.ItemsDao;
import com.xfx.model.Item;

@Repository
public class ItemsServiceImpl implements ItemsService {
	@Autowired
	private ItemsDao itemsDao;

	public void addItem(Item item) {
		itemsDao.addItem(item);
	}

	public List<Item> listItem() {
		return itemsDao.listItem();
	}

	public void removeItem(Item Item) {

		itemsDao.removeItem(Item);
	}

	public void updateItem(Item Item) {
		itemsDao.updateItem(Item);
	}

	public List<Item> getListByUrl(String url) {
		return itemsDao.getListByUrl(url);

	}

}
