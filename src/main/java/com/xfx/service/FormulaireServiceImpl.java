package com.xfx.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xfx.dao.FormulaireDao;
import com.xfx.model.Formulaire;

@Repository
public class FormulaireServiceImpl implements FormulaireService {
	@Autowired
	private FormulaireDao formulaireDao;

	public void addFormulaire(Formulaire formulaire) {
		formulaireDao.addFormulaire(formulaire);
	}

	public List<Formulaire> listFormulaire() {
		return formulaireDao.listFormulaire();
	}

	public void removeFormulaire(Formulaire formulaire) {
		formulaireDao.removeFormulaire(formulaire);
	}

	public void updateFormulaire(Formulaire formulaire) {
		formulaireDao.updateFormulaire(formulaire);
	}

}
