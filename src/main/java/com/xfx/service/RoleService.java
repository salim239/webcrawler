package com.xfx.service;

import java.util.List;

import com.xfx.model.Role;

public interface RoleService {
	public void addRole(Role role);

	public List<Role> listRole();

	public void removeRole(Role role);

	public void updateRole(Role role);
}
