package com.xfx.service;

import java.util.List;

import com.xfx.model.Item;

public interface ItemsService {
	public void addItem(Item item);

	public List<Item> listItem();

	public void removeItem(Item Item);

	public void updateItem(Item Item);

	public List<Item> getListByUrl(String url);
}
