package com.xfx.service;

import java.util.List;

import com.xfx.model.UserAccount;

public interface UserAccountService {
	public void addUserAccount(UserAccount userAccount);
	public List<UserAccount> listUserAccount();
	public void removeUserAccount(UserAccount userAccount);
	public void updateUserAccount(UserAccount userAccount);
}
