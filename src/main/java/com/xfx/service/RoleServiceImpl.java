package com.xfx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.xfx.dao.RoleDao;
import com.xfx.model.Role;

@Repository
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleDao roleDao;

	public void addRole(Role role) {
		roleDao.addRole(role);
	}

	public List<Role> listRole() {
		return roleDao.listRole();
	}

	public void removeRole(Role role) {
		roleDao.removeRole(role);
	}

	public void updateRole(Role role) {
		roleDao.updateRole(role);
	}

}
