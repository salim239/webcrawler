package com.xfx.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.xfx.model.Role;

@Repository
public class RoleDaoImpl implements RoleDao {
	@Autowired
	private MongoTemplate mongoTemplate;
	public static final String COLLECTION_NAME = "role";

	public void addRole(Role role) {
		if (!mongoTemplate.collectionExists(Role.class)) {
			mongoTemplate.createCollection(Role.class);
		}
		mongoTemplate.insert(role, COLLECTION_NAME);

	}

	public List<Role> listRole() {
		return mongoTemplate.findAll(Role.class, COLLECTION_NAME);
	}

	public void removeRole(Role role) {
		mongoTemplate.remove(role, COLLECTION_NAME);

	}

	public void updateRole(Role role) {
		mongoTemplate.insert(role, COLLECTION_NAME);

	}

}
