package com.xfx.dao;

import java.util.List;

import com.xfx.model.Item;

public interface ItemsDao {
	public void addItem(Item item);

	public List<Item> listItem();

	public void removeItem(Item Item);

	public void updateItem(Item Item);

	public List<Item> getListByUrl(String url);
}
