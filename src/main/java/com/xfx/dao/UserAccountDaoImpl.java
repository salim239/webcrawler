package com.xfx.dao;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.xfx.model.UserAccount;

@Repository
public class UserAccountDaoImpl implements UserAccountDao {
	@Autowired
	private MongoTemplate mongoTemplate;
	public static final String COLLECTION_NAME = "useraccount1";

	public void addUserAccount(UserAccount userAccount) {
		if (!mongoTemplate.collectionExists(UserAccount.class)) {
			mongoTemplate.createCollection(UserAccount.class);
		}
		userAccount.setId(UUID.randomUUID().toString());
		mongoTemplate.insert(userAccount, COLLECTION_NAME);

	}

	public List<UserAccount> listUserAccount() {
		return mongoTemplate.findAll(UserAccount.class, COLLECTION_NAME);
	}

	public void removeUserAccount(UserAccount userAccount) {
		mongoTemplate.remove(userAccount, COLLECTION_NAME);

	}

	public void updateUserAccount(UserAccount userAccount) {
		mongoTemplate.insert(userAccount, COLLECTION_NAME);

	}

}
