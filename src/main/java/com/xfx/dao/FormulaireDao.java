package com.xfx.dao;

import java.util.List;

import com.xfx.model.Formulaire;

public interface FormulaireDao {
	public void addFormulaire(Formulaire formulaire);

	public List<Formulaire> listFormulaire();

	public void removeFormulaire(Formulaire formulaire);

	public void updateFormulaire(Formulaire formulaire);
}
