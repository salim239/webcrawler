package com.xfx.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.xfx.model.Item;

@Repository
public class ItemsDaoImpl implements ItemsDao {
	@Autowired
	private MongoTemplate mongoTemplate;

	public static final String COLLECTION_NAME = "Item1";

	public void addItem(Item item) {
		if (!mongoTemplate.collectionExists(Item.class)) {
			mongoTemplate.createCollection(Item.class);
		}
		item.setId(UUID.randomUUID().toString());
		mongoTemplate.insert(item, COLLECTION_NAME);

	}

	public List<Item> listItem() {
		return mongoTemplate.findAll(Item.class, COLLECTION_NAME);
	}

	public void removeItem(Item Item) {
		mongoTemplate.remove(Item, COLLECTION_NAME);

	}

	public void updateItem(Item Item) {
		mongoTemplate.insert(Item, COLLECTION_NAME);
	}

	public List<Item> getListByUrl(String url) {
		List<Item> listResultat = new ArrayList<Item>();
		List<Item> list = mongoTemplate.findAll(Item.class, COLLECTION_NAME);
		for (Item e : list) {
			if (e.getUrl().equals(url.trim()))
				listResultat.add(e);
		}
		return listResultat;
	}

}
