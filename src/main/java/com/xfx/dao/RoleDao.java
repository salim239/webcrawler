package com.xfx.dao;

import java.util.List;

import com.xfx.model.Role;

public interface RoleDao {
	public void addRole(Role role);

	public List<Role> listRole();

	public void removeRole(Role role);

	public void updateRole(Role role);
}
