package com.xfx.dao;

import java.util.List;

import com.xfx.model.UserAccount;

public interface UserAccountDao {
	public void addUserAccount(UserAccount userAccount);

	public List<UserAccount> listUserAccount();

	public void removeUserAccount(UserAccount userAccount);

	public void updateUserAccount(UserAccount userAccount);
}
