

### How do I get set up? ###

>File -> Import -> Existing projects into workspace
Browse to the 'css' root directory

>https://www.openshift.com/ 
Ajouter une application >Tomcat 7 (JBoss EWS 2.0)>Create Application (myapp)
Ajouter  MongoDB 2.4 et RockMongo 1.1

>Modifier le fichier Dispatcher.xml 
```
#!java
 <!-- Factory bean that creates the Mongo instance -->
    <bean id="mongo" class="org.springframework.data.mongodb.core.MongoFactoryBean">
        <property name="host" value="*************" />
        <property name="port" value="27017" />


    </bean>
   <bean id="userCredentials" class="org.springframework.data.authentication.UserCredentials">
        <constructor-arg name="username" value="admin"/>
        <constructor-arg name="password" value="****************"/>
    </bean>

    <!-- MongoTemplate for connecting and quering the documents in the database -->
    <bean id="mongoTemplate" class="org.springframework.data.mongodb.core.MongoTemplate">
        <constructor-arg name="mongo" ref="mongo" />
        <constructor-arg name="databaseName" value="local" />
      <constructor-arg name="userCredentials" ref="userCredentials"/> 
    </bean>


```
>new OpenShift Apllication

![1.PNG](https://bitbucket.org/repo/eAe4ae/images/936337519-1.PNG)

>using existing application and choose the application that you create

![2.PNG](https://bitbucket.org/repo/eAe4ae/images/181236180-2.PNG)

>use existing project

![3.PNG](https://bitbucket.org/repo/eAe4ae/images/1198427774-3.PNG)

>finish

![4.PNG](https://bitbucket.org/repo/eAe4ae/images/321458036-4.PNG)